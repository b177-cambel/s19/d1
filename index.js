// console.log("Hello World!");

// [SECTION] Exponent Operator
// An exponent operator is added to simplify the calculation for the exponent o a given number.
const firstNum = Math.pow(8,2);
console.log(firstNum);

// ES6 Update
const secondNum = 8 ** 2;
console.log(secondNum);


// [SECTION] Template Literals
// Allows developer to write strings without using the concatenation

let name = "John";
// Hello John! Welcome to programming!

// let message = "Hello " + name + "! Welcome to programming!";

// console.log("Message without replacement literals: \n" + message);


// Uses backticks(``)
// variables are place inside a placeholder (${})
let message = `Hello ${name}! Welcome to programming!`;

console.log(`Message with template literals: ${message}`);

// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving a problem 8 ** 2 with the solution of ${secondNum}.
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring

const fullName = ["Juan", "Tolits", "Dela Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Tolits Dela Cruz! It's good to see you again.

console.log("Hello " + fullName[0] + " " + fullName[1] + " "  + fullName[2] + "! It's good to see you again.");

// Using Array Destructuring
// Allows us to unpack elements in arrays into distinct variables.
// Allows developer to name array elements with variables instead of using index number.
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

// Using template literals and array destructuring
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's good to see you again.`);

// console.log(`Hello ${fullName.join('')}! It's good to see you again.`);

// [SECTION] Object Destructuring

const person = {
	givenName: "Jane",
	maidenName: "Miles",
	familyName: "Doe"
};

// Jane
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Hello Jane Miles Doe! It's good to see you again.
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
// This should be exact property name of the object.
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);
/*
function getFullName({givenName, maidenName, familyName}){
	console.log(`This is printed inside a function:`);
	console.log(`${givenName} ${maidenName} ${familyName}`)
}*/

// Using arrow function

const getFullName = ({givenName, maidenName, familyName}) =>{
	console.log(`This is printed inside a function:`)
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// [SECTION] arrow function
/*
	let/const variableName = ()=>{
	statements;
	}
*/

const hello= () => {
	console.log(`Hello World!`);
}
hello ();


// Arrow function with loops
const students = ["John", "Jane", "Judy"];

// John is a student.
// Jane is a student.
// Judy is a student.

students.forEach((student) => {
	console.log(`${student} is a student.`)
	})

// [SECTION] Implicit Return Statement

const add = (x,y) => x + y;

let total = add(1, 2);
console.log(total);

// [SECTION] Default Function Argument Value

const greet = (name = 'User') => {
	return `Good evening, ${name}!`
}

console.log(greet());
console.log(greet("John"));

// [SECTION] Class-Based Object Blueprint

// Creating a class
class Car{
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
const myCar = new Car();

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = "2021"
myCar.color = "White";
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);

/*const car {
	brand: "Ford",
	name: "Ranger Raptor",
	year: 2021,
}

const car1 {
	brand: "Toyota",
	name: "Vios",
	year: 2021,
}*/


